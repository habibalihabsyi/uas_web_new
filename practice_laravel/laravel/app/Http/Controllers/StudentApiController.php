<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;

class StudentApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = User::all()->toJson(JSON_PRETTY_PRINT);
        return response($students, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(),[
            'nim' => 'required|size:8,unique:users',
            'nama' => 'required|min:3|max:50',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required',
            'jenis_kelamin' => 'required',
            'jurusan' => 'required',
            'alamat' => 'required',
            'image' => 'required|file|image|max:10240',

            ]);
            if ($validateData->fails()) {
                return response($validateData->errors(), 400);
                }else{
                    $mahasiswa = new User();
                    $mahasiswa->nim = $request->nim;
                    $mahasiswa->nama = $request->nama;
                    $mahasiswa->email = $request->email;
                    $mahasiswa->username = $request->username;
                    $mahasiswa->password = $request->password;
                    $mahasiswa->jenis_kelamin = $request->jenis_kelamin;
                    $mahasiswa->jurusan = $request->jurusan;
                    $mahasiswa->alamat = $request->alamat;

                if($request->hasFile('image'))
                {
                    $extFile = $request->image->getClientOriginalExtension();
                    $namaFile = 'user-'.time().".".$extFile;
                    $path = $request->image->move('assets/images',$namaFile);
                    $mahasiswa->image = $path;
                }
    
                $mahasiswa->save();
                return response()->json([
                "message" => "student record created"
                ], 201);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (User::where('id', $id)->exists()) {
            $validateData = Validator::make($request->all(), [
            'nim' => 'required|size:8,unique:users',
            'nama' => 'required|min:3|max:50',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required',
            'jenis_kelamin' => 'required',
            'jurusan' => 'required',
            'alamat' => 'required',
            'image' => 'required|file|image|max:1000'
            ]);
            if ($validateData->fails()) {
                return response($validateData->errors(), 400);
                }else{
            
                    $mahasiswa = User::find($id);
                    $mahasiswa->nim = $request->nim;
                    $mahasiswa->nama = $request->nama;
                    $mahasiswa->email = $request->email;
                    $mahasiswa->username = $request->username;
                    $mahasiswa->password = $request->password;
                    $mahasiswa->jenis_kelamin = $request->jenis_kelamin;
                    $mahasiswa->jurusan = $request->jurusan;
                    $mahasiswa->alamat = $request->alamat;

                if($request->hasFile('image'))
                {
                    $extFile = $request->image->getClientOriginalExtension();
                    $namaFile = 'user-'.time().".".$extFile;
                    File::delete($mahasiswa->image);
                    $path = $request->image->move('assets/images',$namaFile);
                    $mahasiswa->image = $path;
                }
    
                $mahasiswa->save();
                    return response()->json([
                    "message" => "student record updated"
                    ], 201);
                    }
                    } else {
                    return response()->json([
                    "message" => "Student not found"
                    ], 404);
                    }
                }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
