<?php

namespace App\Http\Controllers;
use App\User;
use App\Job;
use Illuminate\Http\Request;
use File;

class UserController extends Controller
{
    public function create(){
        return view('register.index');
    }
    
    public function show($student_id)
    {
        $result = Student::findOrFail($student_id);
        return view('student.show',['student' => $result]);
    }
    
    public function store(Request $request)
    {
        $validateData = $request->validate([
        'nim' => 'required|size:8,unique:users',
        'nama' => 'required|min:3|max:50',
        'email' => 'required',
        'username' => 'required',
        'password' => 'required',
        'jenis_kelamin' => 'required',
        'jurusan' => 'required',
        'alamat' => 'required',
        'image' => 'required|file|image|max:10240',
        // 'nim_student' => 'required|size:8,unique:jobs',
        // 'nik_ayah' => 'required|size:14',
        // 'nik_ibu' => 'required|size:14',
        // 'nama_ayah' => 'required|min:3|max:50',
        // 'nama_ibu' => 'required|min:3|max:50',
        // 'alamat_ortu' => 'required',
        // 'pekerjaan_ayah' =>'required',
        // 'pekerjaan_ibu' =>'required',
        // 'faktor' =>'required',
        // 'income_before'=>'required',
        // 'income_after'=>'required',
        // 'image_income_before' => 'required|file|image|max:1000',
        // 'image_income_after' => 'required|file|image|max:1000',
        ]);
        
        // $a = new Job();
        // $a->nim_student = $validateData['nim_student'];
        // $a->nik_ayah = $validateData['nik_ayah'];
        // $a->nik_ibu = $validateData['nik_ibu'];
        // $a->nama_ayah = $validateData['nama_ayah'];
        // $a->nama_ibu = $validateData['nama_ibu'];
        // $a->alamat_ortu = $validateData['alamat_ortu'];
        // $a->pekerjaan_ayah = $validateData['pekerjaan_ayah'];
        // $a->pekerjaan_ibu = $validateData['pekerjaan_ibu'];
        // $a->faktor = $validateData['faktor'];
        // $a->income_before = $validateData['income_before'];
        // $a->income_after = $validateData['income_after'];

        
        $b = new User();
        $b ->nim = $validateData['nim'];
        $b ->nama = $validateData['nama'];
        $b ->email = $validateData['email'];
        $b ->username = $validateData['username'];
        $b ->password = $validateData['password'];
        $b ->jenis_kelamin = $validateData['jenis_kelamin'];
        $b ->jurusan = $validateData['jurusan'];
        $b ->alamat = $validateData['alamat'];
        

        // if($request->hasFile('image_income_before'))
        // {
        //     $extFile = $request->image_income_before->getClientOriginalExtension();
        //     $namaFile = 'user-'.time().".".$extFile;
        //     $path = $request->image_income_before->move('assets/images',$namaFile);
        //     $a->image_income_before = $path;
        // }
        // if($request->hasFile('image_income_after'))
        // {
        //     $extFile = $request->image_income_after->getClientOriginalExtension();
        //     $namaFile = 'user-'.time().".".$extFile;
        //     $path = $request->image_income_after->move('assets/images',$namaFile);
        //     $a->image_income_after = $path;
        // }
        if($request->hasFile('image'))
        {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->image->move('assets/images',$namaFile);
            $b->image = $path;
        }

        // $mahasiswa =$a;
        $mahasiswi = $b;
        

        // $mahasiswa->save();
        $mahasiswi->save();
        $request->session()->flash('pesan','Penambahan data berhasil');
        return redirect()->route('register.index');
    }

    public function index()
    {
        $mahasiswas = User::all();
        return view('register.index',['users' => $mahasiswas]);
    }
    
    public function edit($student_id)
    {
        $result = Student::findOrFail($student_id);
        return view('student.edit',['student' => $result]);
    }

    public function update(Request $request, Student $student)
    {
        $validateData = $request->validate([
        'nim' => 'required|size:8,unique:students',
        'nama' => 'required|min:3|max:50',
        'jenis_kelamin' => 'required',
        'jurusan' => 'required',
        'alamat' => 'required',
        'image' => 'file|image|max:10240',
        ]);
        
        $student->nim = $validateData['nim'];
        $student->name = $validateData['nama'];
        $student->gender = $validateData['jenis_kelamin'];
        $student->departement = $validateData['jurusan'];
        $student->address = $validateData['alamat'];
        if($request->hasFile('image'))
        {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            File::delete($student->image);
            $path = $request->image->move('assets/images',$namaFile);
            $student->image = $path;
        }
        $student->save();
        $request->session()->flash('pesan','Perubahan data berhasil');
        return redirect()->route('student.show',['student' => $student->id]);
    }

    public function destroy(Request $request, Student $student)
    {
        File::delete($student->image);
        $student->delete();
        $request->session()->flash('pesan','Hapus data berhasil');
        return redirect()->route('student.index');
    }

}
