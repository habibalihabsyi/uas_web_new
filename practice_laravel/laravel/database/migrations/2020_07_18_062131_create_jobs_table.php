<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('nim_student');
            $table->bigInteger('nik_ayah');
            $table->bigInteger('nik_ibu');
            $table->string('nama_ayah');
            $table->string('nama_ibu');
            $table->text('alamat')->nullable();
            $table->string('pekerjaan_ayah');
            $table->string('pekerjaan_ibu');
            $table->string('faktor');
            $table->integer('income_before');
            $table->integer('income_after');
            $table->string('image_income_before')->nullable();
            $table->string('image_income_after')->nullable();
            $table->timestamps();

            $table->foreign('nim_student')->references('nim')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
