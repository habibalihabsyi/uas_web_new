<?php

namespace App\Http\Controllers;
use App\Admin;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('login.login');
    }
    
    public function process(Request $request){
        $validateData = $request->validate([
        'username' => 'required',
        'password' => 'required',
        
        ]);
        $result = User::where('username', '=', $validateData['username'])->first();
        $result2 = Admin::where('username', '=', $validateData['username'])->first();
        
        
        if($result2){
            if (($request->password == $result2->password))
            {
                session(['username' => $request->username]);
                return redirect('/adminlte/student');
            }
        }

         else if($result){

            if (($request->password == $result->password))
            {
                session(['username' => $request->username]);
                
                return redirect()->route('userlte.show',['user' => $result]);
            }    

        }

        else{
            return back()->withInput()->with('pesan',"Login Gagal");
        }
    }

    
    public function logout(){
        session()->forget('username');
        return redirect('login')->with('pesan','Logout berhasil');
    }
    
}