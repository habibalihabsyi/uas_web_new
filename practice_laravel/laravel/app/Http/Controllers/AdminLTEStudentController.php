<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use File;
use App\Job;
use Illuminate\Support\Facades\DB;

class AdminLTEStudentController extends Controller
{
    public function create(){
        return view('adminlte.student.create');
    }
    public function createjob(){
        return view('adminlte.student.createjob');
    }
    
    public function show($student_id)
    {
        $result = User::findOrFail($student_id);
        return view('adminlte.student.show',['student' => $result]);
    }
    public function showjob($student_id)
    {
        $result1 = User::findOrFail($student_id);
        $result2 = Job::findOrFail($student_id);
        return view('adminlte.student.job.showjob',['student' => $result1], ['pekerjaan'=> $result2]);
    }
    
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nim' => 'required|size:8,unique:users',
            'nama' => 'required|min:3|max:50',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required',
            'jenis_kelamin' => 'required',
            'jurusan' => 'required',
            'alamat' => 'required',
            'image' => 'required|file|image|max:10240',

            ]);
            
            $mahasiswa = new User();
            $mahasiswa->nim = $validateData['nim'];
            $mahasiswa->nama = $validateData['nama'];
            $mahasiswa->email = $validateData['email'];
            $mahasiswa->username = $validateData['username'];
            $mahasiswa->password = $validateData['password'];
            $mahasiswa->jenis_kelamin = $validateData['jenis_kelamin'];
            $mahasiswa->jurusan = $validateData['jurusan'];
            $mahasiswa->alamat = $validateData['alamat'];

            if($request->hasFile('image'))
            {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-'.time().".".$extFile;
                $path = $request->image->move('assets/images',$namaFile);
                $mahasiswa->image = $path;
            }
    
            $mahasiswa->save();
            $request->session()->flash('pesan','Penambahan data berhasil');
            return redirect()->route('adminlte.student.index');
        }
        
        public function index()
        {
            $mahasiswas = User::all();
            return view('adminlte.student.index',['students' => $mahasiswas]);
        }

        
        public function storejob(Request $request)
        {
            $validateData = $request->validate([
                'nim_student' => 'required|size:8,unique:users',
                'nik_ayah' => 'required|size:14,unique:jobs',
                'nik_ibu' => 'required|size:14',
                'nama_ayah' => 'required|min:3|max:50',
                'nama_ibu' => 'required|min:3|max:50',
                'alamat_ortu' => 'required',
                'pekerjaan_ayah' =>'required',
                'pekerjaan_ibu' =>'required',
                'faktor' =>'required',
                'income_before'=>'required',
                'income_after'=>'required',
                'image_income_before' => 'required|file|image|max:1000',
                'image_income_after' => 'required|file|image|max:1000',
            ]);
            
            $pekerjaan = new Job();
            $pekerjaan->nim_student = $validateData['nim_student'];
            $pekerjaan->nik_ayah = $validateData['nik_ayah'];
            $pekerjaan->nik_ibu = $validateData['nik_ibu'];
            $pekerjaan->nama_ayah = $validateData['nama_ayah'];
            $pekerjaan->nama_ibu = $validateData['nama_ibu'];
            $pekerjaan->alamat_ortu = $validateData['alamat_ortu'];
            $pekerjaan->pekerjaan_ayah = $validateData['pekerjaan_ayah'];
            $pekerjaan->pekerjaan_ibu = $validateData['pekerjaan_ibu'];
            $pekerjaan->faktor = $validateData['faktor'];
            $pekerjaan->income_before = $validateData['income_before'];
            $pekerjaan->income_after = $validateData['income_after'];
            if($request->hasFile('image_income_before'))
            {
                $extFile = $request->image_income_before->getClientOriginalExtension();
                $namaFile = 'user-'.time().".".$extFile;
                $path = $request->image_income_before->move('assets/images',$namaFile);
                $pekerjaan->image_income_before = $path;
            }
            if($request->hasFile('image_income_after'))
            {
                $extFile = $request->image_income_after->getClientOriginalExtension();
                $namaFile = 'user-'.time().".".$extFile;
                $path = $request->image_income_after->move('assets/images',$namaFile);
                $pekerjaan->image_income_after = $path;
            }
            $pekerjaan->save();
            $request->session()->flash('pesan','Penambahan data berhasil');
            return redirect()->route('adminlte.student.indexjob');
        }


    public function indexjob()
    {
        $mahasiswas = Job::all();
        return view('adminlte.student.indexjob',['students' => $mahasiswas]);
    }
    public function dashboard()
    {
        $countStudent['jumlah_mahasiswa'] = DB::table('users')->count();
        $countJob['jumlah_pekerjaan'] = DB::table('jobs')->count();
        return view('adminlte.student.dashboard',['countStudent' => $countStudent], ['countJob' => $countJob]);
    }
    
    public function edit($student_id)
    {
        $result = User::findOrFail($student_id);
        return view('adminlte.student.edit',['student' => $result]);
    }
    public function editjob($student_id)
    {
        $result1 = User::findOrFail($student_id);
        $result2 = Job::findOrFail($student_id);
        return view('adminlte.student.job.editjob',['student' => $result1],['pekerjaan'=> $result2]);
    }

    public function update(Request $request, User $student)
    {
        $validateData = $request->validate([
            'nim' => 'required|size:8,unique:users',
            'nama' => 'required|min:3|max:50',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required',
            'jenis_kelamin' => 'required',
            'jurusan' => 'required',
            'alamat' => 'required',
            'image' => 'required|file|image|max:1000'
            ]);
            
            $student->nim = $validateData['nim'];
            $student->nama = $validateData['nama'];
            $student->email = $validateData['email'];
            $student->username = $validateData['username'];
            $student->password = $validateData['password'];
            $student->jenis_kelamin = $validateData['jenis_kelamin'];
            $student->jurusan = $validateData['jurusan'];
            $student->alamat = $validateData['alamat'];
            
    
            if($request->hasFile('image'))
            {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-'.time().".".$extFile;
                File::delete($student->image);
                $path = $request->image->move('assets/images',$namaFile);
                $student->image = $path;
            }


        $student->save();
        $request->session()->flash('pesan','Perubahan data berhasil');
        return redirect()->route('adminlte.student.show',['student' => $student->id]);
        }
        public function updatejob(Request $request, Job $pekerjaan)
        {
            $validateData = $request->validate([
                'id'=> 'required',
                'nim_student' => 'required|size:8,unique:users',
                'nik_ayah' => 'required|size:14,unique:jobs',
                'nik_ibu' => 'required|size:14',
                'nama_ayah' => 'required|min:3|max:50',
                'nama_ibu' => 'required|min:3|max:50',
                'alamat_ortu' => 'required',
                'pekerjaan_ayah' =>'required',
                'pekerjaan_ibu' =>'required',
                'faktor' =>'required',
                'income_before'=>'required',
                'income_after'=>'required',
                'image_income_before' => 'required|file|image|max:1000',
                'image_income_after' => 'required|file|image|max:1000',
            ]);
                $pekerjaan->id = $validateData['id'];
                $pekerjaan->nim_student = $validateData['nim_student'];
                $pekerjaan->nik_ayah = $validateData['nik_ayah'];
                $pekerjaan->nik_ibu = $validateData['nik_ibu'];
                $pekerjaan->nama_ayah = $validateData['nama_ayah'];
                $pekerjaan->nama_ibu = $validateData['nama_ibu'];
                $pekerjaan->alamat_ortu = $validateData['alamat_ortu'];
                $pekerjaan->pekerjaan_ayah = $validateData['pekerjaan_ayah'];
                $pekerjaan->pekerjaan_ibu = $validateData['pekerjaan_ibu'];
                $pekerjaan->faktor = $validateData['faktor'];
                $pekerjaan->income_before = $validateData['income_before'];
                $pekerjaan->income_after = $validateData['income_after'];
                
        
                if($request->hasFile('image_income_before'))
                {
                    $extFile = $request->image_income_before->getClientOriginalExtension();
                    $namaFile = 'user-'.time().".".$extFile;
                    File::delete($pekerjaan->image_income_before);
                    $path = $request->image_income_before->move('assets/images',$namaFile);
                    $pekerjaan->image_income_before = $path;
                }
                if($request->hasFile('image_income_after'))
                {
                    $extFile = $request->image_income_after->getClientOriginalExtension();
                    $namaFile = 'user-'.time().".".$extFile;
                    File::delete($pekerjaan->image_income_after);
                    $path = $request->image_income_after->move('assets/images',$namaFile);
                    $pekerjaan->image_income_after = $path;
                }


            $pekerjaan->save();
            $request->session()->flash('pesan','Perubahan data berhasil');
            return redirect()->route('adminlte.student.job.showjob',['student' => $pekerjaan->id]);
        }
    

    public function destroy(Request $request, User $student)
    {
        File::delete($student->image);
        $student->delete();
        $request->session()->flash('pesan','Hapus data berhasil');
        return redirect()->route('adminlte.student.index');
    }
}