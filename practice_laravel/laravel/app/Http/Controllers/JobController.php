<?php

namespace App\Http\Controllers;
use App\Job;
use Illuminate\Http\Request;
use File;

class JobController extends Controller
{
    public function create(){
        return view('register.index');
    }
    
    public function show($student_id)
    {
        $result = Student::findOrFail($student_id);
        return view('student.show',['student' => $result]);
    }

    public function storejob(Request $request)
    {
        $validateData = $request->validate([
        'nikayah' => 'required|size:14,unique:jobs',
        'nikibu' => 'required|size:14',
        'namaayah' => 'required|min:3|max:50',
        'namaibu' => 'required|min:3|max:50',
        'alamat' => 'required',
        'ayah' =>'required',
        'ibu' =>'required',
        'faktor' =>'required',
        'income_before'=>'required',
        'income_after'=>'required',
        'image_income_before' => 'required|file|image|max:1000',
        'image_income_after' => 'required|file|image|max:1000',
        ]);
        
        $pekerjaan = new Job();
        $pekerjaan->nikayah = $validateData['nikayah'];
        $pekerjaan->nikibu = $validateData['nikibu'];
        $pekerjaan->namaayah = $validateData['namaayah'];
        $pekerjaan->namaibu = $validateData['namaibu'];
        $pekerjaan->alamat = $validateData['alamat'];
        $pekerjaan->ayah = $validateData['ayah'];
        $pekerjaan->ibu = $validateData['ibu'];
        $pekerjaan->faktor = $validateData['faktor'];
        $pekerjaan->income_before = $validateData['income_before'];
        $pekerjaan->income_after = $validateData['income_after'];
        if($request->hasFile('image_income_before'))
        {
            $extFile = $request->image_income_before->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->image_income_before->move('assets/images',$namaFile);
            $pekerjaan->image_income_before = $path;
        }
        if($request->hasFile('image_income_after'))
        {
            $extFile = $request->image_income_after->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->image_income_after->move('assets/images',$namaFile);
            $pekerjaan->image_income_after = $path;
        }
        $pekerjaan->save();
        $request->session()->flash('pesan','Penambahan data berhasil');
        return redirect()->route('adminlte.student.indexjob');
    }


    public function indexjob()
    {
        $mahasiswas = Job::all();
        return view('adminlte.student.indexjob',['students' => $mahasiswas]);
    }
}