<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<!--===============================================================================================-->
    <link rel="stylesheet" href="../register/fonts/material-icon/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" href="../register/css/style.css">
<!--===============================================================================================-->

    <title>Register</title>
</head>
    <!-- Font Icon -->

<body >

<div class="main">

<div class="container">
    <form action="{{ route('register.store') }}" method="POST" enctype="multipart/form-data" class="appointment-form" id="appointment-form">
    @csrf
        <h1 class="fontss">REGISTER</h1>
        <br>
        
        <div class="form-group-1">

            <label for="nim">NIM</label>
                    <input type="text" class="form-control @error('nim') is-invalid @enderror" id="nim"
                        name="nim" value="{{ old('nim') }}">
                    @error('nim')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                    <label for="nama">Nama Lengkap</label>
                    <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama"
                        name="nama" value="{{ old('nama') }}">
                    @error('nama')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                    <label for="jurusan">Jurusan</label>
                        <select class="form-control" name="jurusan" id="jurusan">
                            <option value="Teknik Informatika"
                                {{ old('jurusan')=='Teknik Informatika' ? 'selected': '' }}>
                                Teknik Informatika
                            </option>
                            <option value="Sistem Informasi"
                                {{ old('jurusan')=='Sistem Informasi' ? 'selected': '' }}>
                                Sistem Informasi
                            </option>
                            <option value="Ilmu Komputer" {{ old('jurusan')=='Ilmu Komputer' ? 'selected': '' }}>
                                Ilmu Komputer
                            </option>
                            <option value="Teknik Komputer"
                                {{ old('jurusan')=='Teknik Komputer' ? 'selected': '' }}>
                                Teknik Komputer
                            </option>
                            <option value="Teknik Telekomunikasi"
                                {{ old('jurusan')=='Teknik Telekomunikasi' ? 'selected': '' }}>
                                Teknik Telekomunikasi
                            </option>
                        </select>
                        @error('jurusan')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror

                    <label for="jenis_kelamin">Jenis Kelamin</label>
                        <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                            <option value="Laki-Laki"
                                {{ old('jenis_kelamin')=='Laki-Laki L' ? 'selected': '' }}>
                                Laki-Laki
                            </option>
                            <option value="Perempuan"
                                {{ old('jenis_kelamin')=='Perempuan P' ? 'selected': '' }}>
                                Perempuan
                            </option>
                        </select>
                        @error('jenis_kelamin')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    
                    <div class="from-group">
                    <label for="alamat">Alamat</label>
                        <br>
                        <textarea class="form-control" id="alamat" rows="5" 
                        name="alamat">{{ old('alamat') }}</textarea>
                    </div>
                    <br>
                    <label for="email">Email</label>
                    <input type="text" class="form-control @error('email') is-invalid @enderror" id="email"
                        name="email" value="{{ old('email') }}">
                    @error('email')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                    <label for="username">Username</label>
                    <input type="text" class="form-control @error('username') is-invalid @enderror" id="username"
                        name="username" value="{{ old('username') }}">
                    @error('username')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                    <label for="password">Password</label>
                    <input type="text" class="form-control @error('password') is-invalid @enderror" id="password"
                        name="password" value="{{ old('password') }}">
                    @error('password')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                    
                    <label for="image">Gambar Profile</label>
                    <input type="file" class="form-control-file" id="image" name="image">
                    @error('image')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                    {{--  <label for="nim_student">NIM</label>
                    <input type="text" class="form-control @error('nim_student') is-invalid @enderror" id="nim"
                        name="nim_student" value="{{ old('nim_student') }}">
                    @error('nim_student')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                            <label for="nik_ayah">NIK</label>
                            <input type="text" class="form-control @error('nik_ayah') is-invalid @enderror" id="nikayah"
                                name="nik_ayah" value="{{ old('nik_ayah') }}">
                            @error('nik_ayah')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="namaayah">Nama Ayah</label>
                            <input type="text" class="form-control @error('nama_ayah') is-invalid @enderror" id="nama_ayah"
                                name="nama_ayah" value="{{ old('nama_ayah') }}">
                            @error('nama_ayah')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror</div>
                            <div class="form-group">
                            <label for="nik_ibu">NIK</label>
                            <input type="text" class="form-control @error('nik_ibu') is-invalid @enderror" id="nik_ibu"
                                name="nik_ibu" value="{{ old('nik_ibu') }}">
                            @error('nik_ibu')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                            <div class="form-group">
                            <label for="nama_ibu">Nama Ibu</label>
                            <input type="text" class="form-control @error('nama_ibu') is-invalid @enderror" id="nama_ibu"
                                name="nama_ibu" value="{{ old('nama_ibu') }}">
                            @error('nama_ibu')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror</div>

                        <div class="form-group">
                            <label for="alamat_ortu">Alamat</label>
                            <br>
                            <textarea class="form-control" id="alamat_ortu" rows="5"
                                name="alamat_ortu">{{ old('alamat_ortu') }}</textarea>
                        
                        </div>

                        <div class="form-group">
                            <br>
                            <label for="pekerjaan_ayah">Pekerjaan Ayah</label>
                            <select class="form-control" name="pekerjaan_ayah" id="pekerjaan_ayah">
                                <option value="Pegawai Negeri"
                                    {{ old('pekerjaan_ayah')=='Pegawai Negeri' ? 'selected': '' }}>
                                    Pegawai Negeri
                                </option>
                                <option value="Wiraswasta"
                                    {{ old('pekerjaan_ayah')=='Salah Satu OrangTua Meninggal Akibat Covid-19' ? 'selected': '' }}>
                                    Wiraswasta
                            </select>
                            @error('pekerjaan_ayah')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <br>
                            <label for="pekerjaan_ibu">Pekerjaan Ibu</label>
                            <select class="form-control" name="pekerjaan_ibu" id="pekerjaan_ibu">
                                <option value="Pegawai Negeri"
                                    {{ old('pekerjaan_ibu')=='Pegawai Negeri' ? 'selected': '' }}>
                                    Pegawai Negeri
                                </option>
                                <option value="Wiraswasta"
                                    {{ old('pekerjaan_ibu')=='Wira swasta' ? 'selected': '' }}>
                                    Wiraswasta
                                </option>
                                <option value="Ibu Rumah Tangga"
                                    {{ old('pekerjaan_ibu')=='Ibu Rumah Tangga' ? 'selected': '' }}>
                                    ibu Rumah Tangga
                                </option>
                            </select>
                            @error('ibu')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="income_before">Pendapatan Sebelum Pandemi (Total)</label>
                            <input type="text" class="form-control @error('income_before') is-invalid @enderror" id="income_before"
                                name="income_before" value="{{ old('income_before') }}">
                            @error('income_before')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>



                        <div class="form-group">
                            <label for="income_after">Pendapatan Setelah Pandemi (Total)</label>
                            <input type="text" class="form-control @error('income_after') is-invalid @enderror" id="income_after"
                                name="income_after" value="{{ old('income_after') }}">
                            @error('income_after')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="faktor">Faktor Penghambat</label>
                            <select class="form-control" name="faktor" id="faktor">
                                <option value="OrangTua Di PHK"
                                    {{ old('faktor')=='OrangTua Di PHK' ? 'selected': '' }}>
                                    OrangTua Di PHK
                                </option>
                                <option value="Salah Satu OrangTua Meninggal Akibat Covid-19"
                                    {{ old('faktor')=='Salah Satu OrangTua Meninggal Akibat Covid-19' ? 'selected': '' }}>
                                    Salah Satu OrangTua Meninggal Akibat Covid-19
                                </option>
                                <option value="OrangTua Meninggal Akibat Covid-19" {{ old('faktor')=='OrangTua Meninggal Akibat Covid-19' ? 'selected': '' }}>
                                OrangTua Meninggal Akibat Covid-19
                                </option>
                            </select>
                            @error('faktor')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="image_income_before">Bukti Pendapatan Sebelum Pandemi</label>
                            <input type="file" class="form-control-file" id="image_income_before" name="image_income_before">
                            @error('image_income_before')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="image_income_after">Bukti Pendapatan Setelah Pandemi</label>
                            <input type="file" class="form-control-file" id="image_income_after" name="image_income_after">
                            @error('image_income_after')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>  --}}
           
        </div>
            <div class="form-submit">
                    <input type="submit" name="submit" id="submit" class="submit" value="Register" />
            </div>
            <div class="form-submit">
                        <a href="{{ url('') }}/login" class="submit"><h4>Login</h4></a>
            </div>
        </div>

        
        
    </form>
</div>

</div>

<!-- JS -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="js/main.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>