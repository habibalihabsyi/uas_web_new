<!DOCTYPE html>
<html lang="en">
<head>
	<title>User - Biodata</title>
	<meta charset="UTF-8">
	<meta name="description" content="Civic - CV Resume">
	<meta name="keywords" content="resume, civic, onepage, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,400i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="../user/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../user/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="../user/css/flaticon.css"/>
	<link rel="stylesheet" href="../user/css/owl.carousel.css"/>
	<link rel="stylesheet" href="../user/css/magnific-popup.css"/>
	<link rel="stylesheet" href="../user/css/style.css"/>


	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	
	
	<!-- <div id="preloder">
		<div class="loader"></div>
	</div> -->
	
	<!-- Header section start -->
	<header class="header-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4">
				<div class="site-logo">
					<br>
					<h2>{{$users->nama}}</h2>
					<p> Profil</p>
				</div>
				</div>
				<div class="col-md-8 text-md-right header-buttons">
					<a href="#" class="site-btn">Pendataan</a>
					<a href="{{ url('') }}/logout" class="site-btn">Sign out</a>
				</div>
			</div>
		</div>
	</header>
	<!-- Header section end -->

	<!-- Hero section start -->
	<section class="hero-section spad">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-10 offset-xl-1">
					<div class="row">
						<div class="col-lg-6">
							<div class="hero-text">
								<h2>{{$users->nama}}</h2>
							</div>
							<div class="hero-info">
								<h2>General Info</h2>
								<ul>
									<li><span>Nim</span>{{$users->nim}} </li>
									<li><span>Jurusan</span>{{$users->jurusan}}</li>
									<li><span>Jenis Kelamin</span>{{$users->jenis_kelamin == 'P' ? 'Perempuan' : 'Laki-laki'}}</li>
									<li><span>Alamat </span>{{$users->alamat}}</li>
									<li><span>Email </span>{{$users->email}}</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-6">
							<figure class="hero-image">
								<img src="{{url('')}}/{{$users->image}}" alt="User Image">
							</figure>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Hero section end -->

	<!-- Social links section start -->
	<div class="social-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-10 offset-xl-1">
					<div class="social-link-warp">
						<div class="social-links">
							<h2>Stay Home</h2>
						</div>
						<h2 class="hidden-md hidden-sm">Virus Covid-19</h2>
					</div>
				</div>
			</div>
		</div>
	</div>	



	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/magnific-popup.min.js"></script>
	<script src="js/circle-progress.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>