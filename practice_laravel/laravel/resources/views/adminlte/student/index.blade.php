@extends('admin_layout.app')
@section('header')
@include('admin_layout.header')
@endsection
@section('leftbar')
@include('admin_layout.leftbar')
@endsection
@section('rightbar')
@include('admin_layout.rightbar')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container mt-3">
            <div class="row">
                <div class="col-12">
                    <div class="py-4 d-flex justify-content-end align-items-center">
                        <h2 class="mr-auto">Data Mahasiswa</h2>
                        <br>
                        <a href="{{ route('adminlte.student.create') }}" class="btn btn-primary">
                            Tambah Mahasiswa
                        </a>
                    </div>
                    @if(session()->has('pesan'))
                    <div class="alert alert-success">
                        {{ session()->get('pesan') }}
                    </div>
                    @endif
                    <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">

                            <tr>
                                <th>#</th>
                                <th>Foto</th>
                                <th>Nim</th>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Jurusan</th>
                                <th>Alamat</th>
                                <th>Email</th>
                            </tr>
                            @forelse ($students as $mahasiswa)
                            <tr>
                                <th>{{$loop->iteration}}</th>
                                <td><img height="30px" src="{{url('')}}/{{$mahasiswa->image}}" class="rounded" alt="">
                                </td>
                                <td><a
                                        href="{{ route('adminlte.student.show',['student' => $mahasiswa->id])}}">{{$mahasiswa->nim}}</a>
                                </td>
                                <td>{{$mahasiswa->nama}}</td>
                                <td>{{$mahasiswa->jenis_kelamin == 'P'?'Perempuan':'Laki-laki'}}</td>
                                <td>{{$mahasiswa->jurusan}}</td>
                                <td>{{$mahasiswa->alamat == '' ? 'N/A' : $mahasiswa->alamat}}</td>
                                <td>{{$mahasiswa->email}}</td>
                            </tr>
                            @empty
                            <td colspan="8" class="text-center">Tidak ada data...</td>
                            @endforelse
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection