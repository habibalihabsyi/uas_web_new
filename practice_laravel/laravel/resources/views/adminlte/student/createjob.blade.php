@extends('admin_layout.app')
@section('header')
@include('admin_layout.header')
@endsection
@section('leftbar')
@include('admin_layout.leftbar')
@endsection
@section('rightbar')
@include('admin_layout.rightbar')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container pt-4 bg-white">
            <div class="row">
                <div class="col-md-8 col-xl-6">
                    <h1>Input Data Pekerjaan Orang Tua Mahasiswa</h1>
                    <hr>
                    <form action="{{ route('adminlte.student.storejob') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="nim_student">NIM Mahasiswa</label>
                            <input type="text" class="form-control @error('nim_student') is-invalid @enderror" id="nim_student"
                                name="nim_student" value="{{ old('nim_student') }}">
                            @error('nim_student')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="nik_ayah">NIK Ayah</label>
                            <input type="text" class="form-control @error('nik_ayah') is-invalid @enderror" id="nik_ayah"
                                name="nik_ayah" value="{{ old('nik_ayah') }}">
                            @error('nik_ayah')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="nama_ayah">Nama Ayah</label>
                            <input type="text" class="form-control @error('nama_ayah') is-invalid @enderror" id="nama_ayah"
                                name="nama_ayah" value="{{ old('nama_ayah') }}">
                            @error('nama_ayah')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror</div>
                            <div class="form-group">
                            <label for="nik_ibu">NIK Ibu</label>
                            <input type="text" class="form-control @error('nik_ibu') is-invalid @enderror" id="nik_ibu"
                                name="nik_ibu" value="{{ old('nik_ibu') }}">
                            @error('nik_ibu')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                            <div class="form-group">
                            <label for="nama_ibu">Nama Ibu</label>
                            <input type="text" class="form-control @error('nama_ibu') is-invalid @enderror" id="nama_ibu"
                                name="nama_ibu" value="{{ old('nama_ibu') }}">
                            @error('nama_ibu')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror</div>

                        <div class="form-group">
                            <label for="alamat_ortu">Alamat</label>
                            <textarea class="form-control" id="alamat_ortu" rows="3"
                                name="alamat_ortu">{{ old('alamat_ortu') }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="pekerjaan_ayah">Pekerjaan Ayah</label>
                            <select class="form-control" name="pekerjaan_ayah" id="pekerjaan_ayah">
                                <option value="Pegawai Negeri"
                                    {{ old('pekerjaan_ayah')=='Pegawai Negeri' ? 'selected': '' }}>
                                    Pegawai Negeri
                                </option>
                                <option value="Wiraswasta"
                                    {{ old('pekerjaan_ayah')=='Salah Satu OrangTua Meninggal Akibat Covid-19' ? 'selected': '' }}>
                                    Wiraswasta
                            </select>
                            @error('ayah')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="pekerjaan_ibu">Pekerjaan Ibu</label>
                            <select class="form-control" name="pekerjaan_ibu" id="pekerjaan_ibu">
                                <option value="Pegawai Negeri"
                                    {{ old('pekerjaan_ibu')=='Pegawai Negeri' ? 'selected': '' }}>
                                    Pegawai Negeri
                                </option>
                                <option value="Wiraswasta"
                                    {{ old('pekerjaan_ibu')=='Wira swasta' ? 'selected': '' }}>
                                    Wiraswasta
                                </option>
                                <option value="Ibu Rumah Tangga"
                                    {{ old('pekerjaan_ibu')=='Ibu Rumah Tangga' ? 'selected': '' }}>
                                    ibu Rumah Tangga
                                </option>
                            </select>
                            @error('ibu')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="income_before">Pendapatan Sebelum Pandemi (Total)</label>
                            <input type="text" class="form-control @error('income_before') is-invalid @enderror" id="income_before"
                                name="income_before" value="{{ old('income_before') }}">
                            @error('income_before')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>



                        <div class="form-group">
                            <label for="income_after">Pendapatan Setelah Pandemi (Total)</label>
                            <input type="text" class="form-control @error('income_after') is-invalid @enderror" id="income_after"
                                name="income_after" value="{{ old('income_after') }}">
                            @error('income_after')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="faktor">Faktor Penghambat</label>
                            <select class="form-control" name="faktor" id="faktor">
                                <option value="OrangTua Di PHK"
                                    {{ old('faktor')=='OrangTua Di PHK' ? 'selected': '' }}>
                                    OrangTua Di PHK
                                </option>
                                <option value="Salah Satu OrangTua Meninggal Akibat Covid-19"
                                    {{ old('faktor')=='Salah Satu OrangTua Meninggal Akibat Covid-19' ? 'selected': '' }}>
                                    Salah Satu OrangTua Meninggal Akibat Covid-19
                                </option>
                                <option value="OrangTua Meninggal Akibat Covid-19" {{ old('faktor')=='OrangTua Meninggal Akibat Covid-19' ? 'selected': '' }}>
                                OrangTua Meninggal Akibat Covid-19
                                </option>
                            </select>
                            @error('faktor')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="image_income_before">Bukti Pendapatan Sebelum Pandemi</label>
                            <input type="file" class="form-control-file" id="image_income_before" name="image_income_before">
                            @error('image_income_before')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="image_income_after">Bukti Pendapatan Setelah Pandemi</label>
                            <input type="file" class="form-control-file" id="image_income_after" name="image_income_after">
                            @error('image_income_after')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary mb-2">Tambahkan</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection