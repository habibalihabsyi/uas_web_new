<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Job;

class MahasiswaController extends Controller
{
    public function create(){
        return view('userlte.create');
    }
    public function createjob(){
        return view('userlte.createjob');
    }
    
    public function show($student_id)
    {
        $result = User::findOrFail($student_id);
        return view('userlte.show',['users' => $result]);
    }
    public function showmore($student_id)
    {
        $result = User::findOrFail($student_id);
        return view('userlte.showmore',['users' => $result]);
    }
    public function storejob(Request $request)
        {
            $validateData = $request->validate([
                'nim_student' => 'required|size:8,unique:jobs',
                'nik_ayah' => 'required|size:14,unique:jobs',
                'nik_ibu' => 'required|size:14',
                'nama_ayah' => 'required|min:3|max:50',
                'nama_ibu' => 'required|min:3|max:50',
                'alamat_ortu' => 'required',
                'pekerjaan_ayah' =>'required',
                'pekerjaan_ibu' =>'required',
                'faktor' =>'required',
                'income_before'=>'required',
                'income_after'=>'required',
                'image_income_before' => 'required|file|image|max:1000',
                'image_income_after' => 'required|file|image|max:1000',
            ]);
            
            $pekerjaan = new Job();
            $pekerjaan->nim_student = $validateData['nim_student'];
            $pekerjaan->nik_ayah = $validateData['nik_ayah'];
            $pekerjaan->nik_ibu = $validateData['nik_ibu'];
            $pekerjaan->nama_ayah = $validateData['nama_ayah'];
            $pekerjaan->nama_ibu = $validateData['nama_ibu'];
            $pekerjaan->alamat_ortu = $validateData['alamat_ortu'];
            $pekerjaan->pekerjaan_ayah = $validateData['pekerjaan_ayah'];
            $pekerjaan->pekerjaan_ibu = $validateData['pekerjaan_ibu'];
            $pekerjaan->faktor = $validateData['faktor'];
            $pekerjaan->income_before = $validateData['income_before'];
            $pekerjaan->income_after = $validateData['income_after'];
            if($request->hasFile('image_income_before'))
            {
                $extFile = $request->image_income_before->getClientOriginalExtension();
                $namaFile = 'user-'.time().".".$extFile;
                $path = $request->image_income_before->move('assets/images',$namaFile);
                $pekerjaan->image_income_before = $path;
            }
            if($request->hasFile('image_income_after'))
            {
                $extFile = $request->image_income_after->getClientOriginalExtension();
                $namaFile = 'user-'.time().".".$extFile;
                $path = $request->image_income_after->move('assets/images',$namaFile);
                $pekerjaan->image_income_after = $path;
            }
            $pekerjaan->save();
            $request->session()->flash('pesan','Penambahan data berhasil');
            return redirect()->route('userlte.show');
        }
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nim' => 'required|size:8,unique:students',
            'nama' => 'required|min:3|max:50',
            'jenis_kelamin' => 'required',
            'jurusan' => 'required',
            'alamat' => 'required',
            'email' => 'required' ,
            'image' => 'file|image|max:10240',
        ]);
        
        $mahasiswa = new User();
        $mahasiswa->nim = $validateData['nim'];
        $mahasiswa->name = $validateData['nama'];
        $mahasiswa->email = $validateData['email'];
        $mahasiswa->username = $validateData['username'];
        $mahasiswa->password = $validateData['password'];

        $mahasiswa->save();
        $request->session()->flash('pesan','Penambahan data berhasil');
        return redirect()->route('userlte.index');
    }

    public function index()
    {
        $mahasiswas = User::all();
        return view('userlte.index',['user' => $mahasiswas]);
    }
    
    public function edit($student_id)
    {
        $result = User::findOrFail($student_id);
        return view('userlte.edit',['student' => $result]);
    }

    public function update(Request $request, Student $student)
    {
        $validateData = $request->validate([
        'nim' => 'required|size:8,unique:students',
        'nama' => 'required|min:3|max:50',
        'jenis_kelamin' => 'required',
        'jurusan' => 'required',
        'alamat' => '',
        'image' => 'file|image|max:10240',
        ]);
        
        $student->nim = $validateData['nim'];
        $student->name = $validateData['nama'];
        $student->jenis_kelamin = $validateData['jenis_kelamin'];
        $student->jurusan = $validateData['jurusan'];
        $student->alamat = $validateData['alamat'];
        $mahasiswa->email = $validateData['email'];
        if($request->hasFile('image'))
        {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            File::delete($student->image);
            $path = $request->image->move('assets/images',$namaFile);
            $student->image = $path;
        }
        $student->save();
        $request->session()->flash('pesan','Perubahan data berhasil');
        return redirect()->route('userlte.show',['student' => $student->id]);
    }

    public function destroy(Request $request, Student $student)
    {
        File::delete($student->image);
        $student->delete();
        $request->session()->flash('pesan','Hapus data berhasil');
        return redirect()->route('student.index');
    }

}
