@extends('admin_layout.app')
@section('header')
@include('admin_layout.header')
@endsection
@section('leftbar')
@include('admin_layout.leftbar')
@endsection
@section('rightbar')
@include('admin_layout.rightbar')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="h2 mr-auto">Biodata {{$student->nama}}</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container mt-3">
            <div class="row">
                <div class="col-12">
                    <div class="pt-3 d-flex justify-content-end align-items-center">
                        <a href="{{ route('adminlte.student.job.editjob',['student' => $student->id]) }}"
                            class="btn btn-primary">Edit
                        </a>
                        <br>
                        <br>
                        <form action="{{ route('adminlte.student.destroy',['student'=>$student->id]) }}" method="POST">
                            @method('DELETE')
                            @csrf

                            <button type="submit" class="btn btn-danger ml-3">Hapus</button>

                        </form>
                    </div>
                    <hr>
                    @if(session()->has('pesan'))
                    <div class="alert alert-success">
                        {{ session()->get('pesan') }}
                    </div>
                    @endif
                    <ul>
                        <li>NIM Mahasiswa: {{$student ->nim}} </li>
                        <li>Nama Mahasiswa: {{$student ->nama}} </li>
                        <li>NIK Ayah: {{$pekerjaan ->nik_ayah}} </li>
                        <li>Nama Ayah: {{$pekerjaan ->nama_ayah}} </li>
                        <li>NIK Ibu: {{$pekerjaan ->nik_ibu}} </li>
                        <li>Nama Ibu: {{$pekerjaan ->nama_ibu}} </li>
                        <li>Alamat:
                            {{$student ->alamat == '' ? 'N/A' : $student ->alamat}}
                        </li>
                        <li>Pekerjaan Ayah: {{$pekerjaan ->pekerjaan_ayah}} </li>
                        <li>Pekerjaan Ibu: {{$pekerjaan ->pekerjaan_ibu}} </li>
                        <li>Total Pendapatan Sebelum Pandemi: {{$pekerjaan ->income_before}} </li>
                        <li>Total Pendapatan Setelah Pandemi: {{$pekerjaan ->income_after}} </li>
                        <li>Faktor Penghambat: {{$pekerjaan ->faktor}} </li>
                        <li>Penurunan Prndapatan: {{$pekerjaan->income_before-$pekerjaan->income_after}} </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection